job('hello-jenkins-job-dsl-test1') {
    scm {
        git('https://wj2016@bitbucket.org/wj2016/myproj.git')
    }
    triggers {
        scm('H/15 * * * *')
    }
    steps {
        shell('echo "haha, test shell"')
        gradle('run_my_demo_suite')
    }

    logRotator(-1, 5, -1, 5)
}
